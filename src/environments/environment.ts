// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyC0B_dc1Ho4UBvMVQkB4Ns-dVK1v4_X23Y",
    authDomain: "oshop-18daf.firebaseapp.com",
    databaseURL: "https://oshop-18daf.firebaseio.com",
    projectId: "oshop-18daf",
    storageBucket: "oshop-18daf.appspot.com",
    messagingSenderId: "1001155139676",
    appId: "1:1001155139676:web:020d1097c8f113e1"
  }
};
