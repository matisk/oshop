import { Component, OnDestroy } from '@angular/core';
import { ProductService } from 'shared/services/product.service';
import { Subscription } from 'rxjs/Subscription';
import { Products } from 'shared/models/products';
import { DataTableResource } from 'angular-4-data-table';

@Component({
    selector: 'app-admin-products',
    templateUrl: './admin-products.component.html',
    styleUrls: ['./admin-products.component.css']
})
export class AdminProductsComponent implements OnDestroy {

    products: Products[];
    subscription: Subscription;
    tableResource: DataTableResource<Products>;
    items: Products[] = [];
    itemsCount: number;

    constructor(
        private productService: ProductService
    ) {
        this.subscription = productService.getAll().subscribe(products => {
            this.products = products;
            this.initializeTable(products);
        });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    private initializeTable(products: Products[]) {
        this.tableResource = new DataTableResource(products);
        this.tableResource.query({ offset: 0 })
            .then(items => this.items = items);
        this.tableResource.count()
            .then(count => this.itemsCount = count);
    }

    filter(query: string) {
        const filteredProducts = (query) ?
            this.products.filter(p => p.title.toLocaleLowerCase().includes(query.toLocaleLowerCase())) :
            this.products;

        this.initializeTable(filteredProducts);
    }

    reloadItems(params) {
        if (!this.tableResource) {
            return;
        }
        this.tableResource.query(params)
            .then(items => this.items = items);
    }
}
