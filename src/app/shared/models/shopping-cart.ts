import { ShoppingCartItem } from './shopping-cart-item';
import { Products } from './products';

export class ShoppingCart {
    items: ShoppingCartItem[] = [];

    constructor(
        private itemsMap: {
            [productId: string]: ShoppingCartItem
        }
    ) {
        this.itemsMap = this.itemsMap || {};
        for (let productId in itemsMap) {
            let item = itemsMap[productId];
            this.items.push(new ShoppingCartItem({ ...item, $key: productId }));
        }
    }

    get totalItemsCount() {
        let count = 0;
        for (let productId in this.itemsMap) {
            count += this.itemsMap[productId].quantity;
        }
        return count;
    }

    get totalPrice() {
        let sum = 0;
        for (let productId in this.items) {
            sum += this.items[productId].totalPrice;
        }
        return sum;
    }

    getQuantity(product: Products) {
        let item = this.itemsMap[product.$key];
        return item ? item.quantity : 0;
    }
}
